<div class="relative  mt-3 md:mt-0" x-data="{ isOpen:true }" @click.away="isOpen= false">
    <input 
    wire:model.debounce.500ms="search" type="text" class="bg-gray-800 text-sm rounded-full w-64 px-4 pl-8  py-1 focus:outline-none focus:shadow-outline" 
    placeholder="search"
    x-ref="search"
    @keydown.window="
    if(event.keyCode === 191)
    {
        event.preventDefault();  
       $refs.search.focus(); 
    }
    "
    @focus="isOpen= true"
    keydown="isOpen= true"
    @keydown.escape.window="isOpen=false"
    @keydown.shift.tab="isOpen= false"

    >
        <div class="absolute top-0">
            <svg class="fill-current w-4 text-gray-500 mt-2 ml-2" viewBox="0 0 24 24">
            <path class="heroicon-ui" d="M18.109,17.776l-3.082-3.081c-0.059-0.059-0.135-0.077-0.211-0.087c1.373-1.38,2.221-3.28,2.221-5.379c0-4.212-3.414-7.626-7.625-7.626c-4.212,0-7.626,3.414-7.626,7.626s3.414,7.627,7.626,7.627c1.918,0,3.665-0.713,5.004-1.882c0.006,0.085,0.033,0.17,0.098,0.234l3.082,3.081c0.143,0.142,0.371,0.142,0.514,0C18.25,18.148,18.25,17.918,18.109,17.776zM9.412,16.13c-3.811,0-6.9-3.089-6.9-6.9c0-3.81,3.089-6.899,6.9-6.899c3.811,0,6.901,3.09,6.901,6.899C16.312,13.041,13.223,16.13,9.412,16.13z"></path>
            </svg>  
        </div>
        <div wire:loading class="spinner top-0 right-0 mr-4 mt-3">

        </div>
        @if(strlen($search) > 2)
        <div class="z-50 absolute bg-gray-800 rounded w-64 mt-4 text-sm" 
        x-show.transition.opacity="isOpen"
         
        
        >
            @if($searchResults->count() > 0)
            <ul>
                @foreach($searchResults  as $result)
                <li class="border-b border-gray-700">
                    <a
                     href="{{ route('movie.show', $result['id'])}}" 
                     class="block hover:bg-gray-700 px-3 py-3 flex items-center transition ease-in-out
                     duration-150"
                     @if($loop->last) 
                     @keydown.tab="isOpen= false"
                     @endif
                     >
                   @if($result['poster_path'])

                    <img src="https://image.tmdb.org/t/p/w92/{{$result['poster_path']}}" alt="poster"
                      class="w-8">

                      @else
                      <img src="https://via.placeholder.com/50x75" alt="poster" class="w-8">
                      @endif
                        <span class="ml-4">{{$result['title']}}</span>
                    </a>
                </li>
                @endforeach
               
            </ul>
            @else
            <div  class="px-3 py-3">No results for "{{$search}}"</div>
            @endif
        </div>
        @endif
</div>