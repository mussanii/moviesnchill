@extends('layouts.main')

@section('content')

<div class="tv-info border-b border-gray-800">

<div class="container mx-auto px-4 py-16 flex flex-col md:flex-row">
    <div class="flex-none">
    <img src="{{$tvshow['poster_path']}}" alt="poster" class=" w-64 lg:w-96">
    </div>
    <div class="md:ml-24">
        <h2 class="text-4xl font-semibold">{{$tvshow['name']}}</h2>


        <div class="flex flex-wrap items-center text-gray-400 text-sm mt-1">
        <svg class="fill-current text-orange-500 w-4"
        viewBox="0 0 24 24"><g data-name="Layer 2">
                <path d="M17.684,7.925l-5.131-0.67L10.329,2.57c-0.131-0.275-0.527-0.275-0.658,0L7.447,7.255l-5.131,0.67C2.014,7.964,1.892,8.333,2.113,8.54l3.76,3.568L4.924,17.21c-0.056,0.297,0.261,0.525,0.533,0.379L10,15.109l4.543,2.479c0.273,0.153,0.587-0.089,0.533-0.379l-0.949-5.103l3.76-3.568C18.108,8.333,17.986,7.964,17.684,7.925 M13.481,11.723c-0.089,0.083-0.129,0.205-0.105,0.324l0.848,4.547l-4.047-2.208c-0.055-0.03-0.116-0.045-0.176-0.045s-0.122,0.015-0.176,0.045l-4.047,2.208l0.847-4.547c0.023-0.119-0.016-0.241-0.105-0.324L3.162,8.54L7.74,7.941c0.124-0.016,0.229-0.093,0.282-0.203L10,3.568l1.978,4.17c0.053,0.11,0.158,0.187,0.282,0.203l4.578,0.598L13.481,11.723z"
                data-name="star" /></g></svg>
            <span class="ml-1">{{$tvshow['vote_average']}}</span>
            <span class="mx-2 "> |</span>
            <span> {{$tvshow['first_air_date']}}</span>
            <span class="mx-2">|</span>
            <span>
           {{$tvshow['genres'] }}
            </span>
        </div>


        <p class="text-gray-300 mt-8">
           {{$tvshow['overview']}}
        </p>


        <div class="mt-12">
       
            <div class="flex mt-4">
                @foreach($tvshow['created_by'] as $crew)

                <div class="mr-8">
                    <div> {{$crew['name']}}</div>
                    <div class="text-sm text-gray-400">creator</div>

                </div>

                @endforeach

            </div>

        </div>
        <div x-data="{isOpen:false}">
       @if(count($tvshow['videos']['results']) > 0)

        <div class="mt-12">
            <button
            @click="isOpen = true"
             href="https://youtube.com/watch?v={{$tvshow['videos']['results']['0']['key']}}" class="flex inline-flex items-center bg-orange-500 text-gray-900 rounded font-semibold
            px-5 py-4 hover:bg-orange-600 transition ease-in-out duration-150" >
            <svg class="w-6 fill-current" viewBox="0 0 24 24">
							<path d="M18.175,4.142H1.951C1.703,4.142,1.5,4.344,1.5,4.592v10.816c0,0.247,0.203,0.45,0.451,0.45h16.224c0.247,0,0.45-0.203,0.45-0.45V4.592C18.625,4.344,18.422,4.142,18.175,4.142 M4.655,14.957H2.401v-1.803h2.253V14.957zM4.655,12.254H2.401v-1.803h2.253V12.254z M4.655,9.549H2.401V7.747h2.253V9.549z M4.655,6.846H2.401V5.043h2.253V6.846zM14.569,14.957H5.556V5.043h9.013V14.957z M17.724,14.957h-2.253v-1.803h2.253V14.957z M17.724,12.254h-2.253v-1.803h2.253V12.254zM17.724,9.549h-2.253V7.747h2.253V9.549z M17.724,6.846h-2.253V5.043h2.253V6.846z" ></path>
						</svg>
                        <span class="ml-2">Play Trailer</span>
</button>
                    </div>
            @endif
            <div
                style="background-color: rgba(0,0,0,.5);"

                class="fixed top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto"
            x-show.transition.opacity="isOpen"
                >
             <div class="container mx-auto lg:px-32 rounded-lg overflow-y-auto">
                 <div class="bg-gray-900 rounded">
                     <div class="flex justify-end pr-4 pt-2">
                         <button @click="isOpen = false" class="text-3xl leading-none hover:text-gray-300">&times;</button>
                     </div>
                     <div class="modal-body px-8 py-8">
                         <div class="responsive-container overflow-hidden relative"
                          style="padding-top: 56.25%">
                        <iframe width="560" height="315"
                         class="responsive-iframe absolute
                          top-0 left-0 w-full h-full"
                        src="https://www.youtube.com/embed/{{$tvshow['videos']['results']['0']['key']}}"
                        style="border:0;" allow="autoplay; encrypted-media"
                         allowfullscreen></iframe>
                        </div>

                     </div>
                  </div>
                 </div>

              </div>
        </div>

            </div>

        </div>


    </div>

    <div class="tv-cast border-b border-gray-800">
    <div class="container mx-auto px-4 py-16">
        <h2 class="text-4xl font-semibold">
            Cast
        </h2>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">

      @foreach($tvshow['cast'] as $cast)

<div class="mt-8">

<a href="{{route('actor.show',$cast['id']) }}">
<img src="{{$cast['profile_path']}}" alt="profile" class="hover:opacity-75 transition ease-in-out duration-150 ">

</a>
<div class="mt-2">
<a href="{{route('actor.show',$cast['id']) }}" class="text-lg mt-2 hover:text-gray:300 "> {{$cast['name']}}</a>
<div class="text-sm text-gray-400">
   as  {{$cast['character']}}


</div>





</div>


</div>

@endforeach
    </div>
</div>
<div class="movie-images" x-data="{isOpen: false, image: ''}">
    <div class="container mx-auto px-4 py-16">
        <h2 class="text-4xl font-semibold">
            Images
        </h2>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3  gap-8">
@foreach ($tvshow['images'] as $image)


<div class="mt-8">
        <a
         href="#"
         @click.prevent="
         isOpen = true
         image='{{'https://image.tmdb.org/t/p/original/'.$image['file_path']}}'"

         >
        <img src="{{'https://image.tmdb.org/t/p/w500/'.$image['file_path']}}" alt="Images" class="hover:opacity-75 transition ease-in-out duration-150 ">

        </a>
</div>

@endforeach

    </div>
    <div
        style="background-color: rgba(0,0,0,.5);"

        class="fixed top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto"
        x-show.transition.opacity="isOpen"
    >
             <div class="container mx-auto lg:px-32 rounded-lg overflow-y-auto">
                 <div class="bg-gray-900 rounded">
                     <div class="flex justify-end pr-4 pt-2">
                         <button

                          @click="isOpen = false"
                          @keydown.escape.window="isOpen = false"
                           class="text-3xl leading-none hover:text-gray-300">&times;</button>
                     </div>
                     <div class="modal-body px-8 py-8">
                        <img :src="image" alt="poster">

                     </div>
                  </div>
                 </div>

              </div>


</div>
</div>




@endsection
