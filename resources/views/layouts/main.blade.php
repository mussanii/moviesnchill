<!DOCTYPE html>
<html >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Movies&Chill</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/main.css">
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
        <livewire:styles>
    </head>
    <body class="font-sans bg-gray-900 text-white">
       <nav class="border-b border-gray-800">
         <div class="container mx-auto px-4 flex flex-col md:flex-row items-center justify-between px-4 py-6">
        <ul class="flex flex-col md:flex-row items-center">
            <li>
                <a href="{{ route('movies.index')}}">
                    <img class="w-32"  src="/images/movie.png" alt="movies & chill" />

                </a>
            </li>
            <li class="md:ml-16  mt-3 md:mt-0">
                <a href="{{ route('movies.index')}}" class="hover:text-gray:300">Movies</a>

            </li>
            <li class="md:ml-6  mt-3 md:mt-0">
                <a href="{{ route('tv.index')}}" class="hover:text-gray:300">Tv Shows</a>

            </li>
            <li class="md:ml-6  mt-3 md:mt-0">
            <a href="{{ route('actors.index')}}" class="hover:text-gray:300">Actors</a>

            </li>
        </ul>

        <div class=" flex flex-col md:flex-row items-center">
            <livewire:search-dropdown>


            <div class="md:ml-4 mt-3 md:mt-0">
                    <a href="#">
                        <img src="/image/shelly.jpg" alt="avatar" class="rounded-full w-8 h-8">
                    </a>

                </div>

        </div>
         </div>
        </nav>

       @yield('content')

       <livewire:scripts>
       @yield('scripts')
    </body>
</html>
