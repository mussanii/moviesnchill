<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Http;



class SearchDropdown extends Component
{
    public $search ='';

    public function render()
    {
        $searchresults=[];
        if(strlen($this->search) > 2){

        $searchresults= Http::withToken(config('services.tmdb.token'))

        ->get('https://api.themoviedb.org/3/search/movie?query='. $this->search)

        ->json()['results'];

        }

       // dump($searchresults);

        return view('livewire.search-dropdown',[
            'searchResults'=>collect($searchresults)->take(8),
        ]);
    }
}
