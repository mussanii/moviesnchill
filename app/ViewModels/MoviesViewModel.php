<?php

namespace App\ViewModels;

use Spatie\ViewModels\ViewModel;
use illuminate\Support\Carbon;

class MoviesViewModel extends ViewModel
{
    public $popularMovies;
    public $genres;
    public $nowPlaying;

    public function __construct($popularMovies, $genres,$nowPlaying)
    {
        $this->popularMovies=$popularMovies;
        $this->genres = $genres;
        $this->nowPlaying = $nowPlaying;
    }

    public function popularMovies(){

    return $this->formatMovies($this->popularMovies);

    }

    public function genres(){
             return collect($this->genres)->mapWithKeys(function($genre){
            return[$genre['id']=>$genre['name']];
        });

    }

    public function nowPlaying(){

        return $this->formatMovies($this->nowPlaying);
    }


    private function formatMovies($movies)
    {

        return collect($movies)->map(function($movie){
            $genresFormatted = collect($movie['genre_ids'])->mapWithKeys(function($value){
                return [$value=>$this->genres()->get($value)];
            })->implode(', ');
            return collect($movie)->merge([
                'poster_path'=> 'https://image.tmdb.org/t/p/w500/'.$movie['poster_path'],
                'vote_average'=>$movie['vote_average']* 10 ."%",
                'release_date'=>Carbon::parse($movie['release_date'])->format('M d,Y'),
                'genres'=>$genresFormatted

            ])->only([
                'poster_path','id','genre_ids','title','vote_average','overview','release_date','genres',
            ]);

        }) ;

    }
}
